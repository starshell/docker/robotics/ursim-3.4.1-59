# Universal Robot Simulator

This image is a base install of Ubuntu-14.04 with the universal robot simulator.

## Usage

```sh
docker run -it --security-opt label=disable \
       --device /dev/dri --env="DISPLAY" \
       --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
       -p 30001-30004:30001-30004  \
       --name="ursim" registry.gitlab.com/starshell/docker/robotics/ursim-3.4.1-59:master start-ursim.sh UR3
```
